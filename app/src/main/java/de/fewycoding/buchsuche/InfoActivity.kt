package de.fewycoding.buchsuche

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_info.*
import java.io.InputStream
import java.net.URL
import java.util.concurrent.ExecutionException

class InfoActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val intent = intent.extras
        titelView.text = intent!!.get("titel") as String?
        autorView.text = intent.get("authors") as String?
        infoView.text = intent.get("Info") as String?
        seitenView.text = "Seitenanzahl: " + (intent.get("seitenZahl") as String?)!!

        val bildURL = intent.get("bild") as String?
        val bilddownloader = Bilddownloader()
        try {
            val bitmap = bilddownloader.execute(bildURL).get()
            bilddownloadView.setImageBitmap(bitmap)
        } catch (e: ExecutionException) {
            val toast = Toast.makeText(this, "Bild konnte nicht geladen werden", Toast.LENGTH_LONG)
            toast.show()

            e.printStackTrace()
        } catch (e: InterruptedException) {
            val toast = Toast.makeText(this, "Bild konnte nicht geladen werden", Toast.LENGTH_LONG)
            toast.show()
            e.printStackTrace()
        }

    }

    @SuppressLint("StaticFieldLeak")
    internal inner class Bilddownloader : AsyncTask<String, Void, Bitmap>() {
        override fun doInBackground(vararg bildURL: String): Bitmap? {
            var inputStream: InputStream? = null
            val bitmap: Bitmap?
            try {
                val url = URL(bildURL[0])
                val connection = url.openConnection()
                connection.connectTimeout = 5000
                connection.readTimeout = 10000
                inputStream = url.openStream()
                bitmap = BitmapFactory.decodeStream(inputStream)
                return bitmap

            } catch (ex: Exception) {
                ex.printStackTrace()

            } finally {
                if (inputStream != null)
                    try {
                        inputStream.close()
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

            }
            return null
        }
    }
}
