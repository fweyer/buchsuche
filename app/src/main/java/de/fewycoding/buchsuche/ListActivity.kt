package de.fewycoding.buchsuche

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_activity.*

class ListActivity : AppCompatActivity() {

    companion object {
        internal val ids = intArrayOf(R.id.titel, R.id.author, R.id.seitenzahl)
        internal val names = arrayOf("title", "authors", "pageCount", "bild","Info")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_activity)
        val apiresults = intent.getParcelableExtra<ApiResults>("apiResults")
        val data = apiresults.data
        listeView.divider = null
        listeView.dividerHeight = 0
        val adapter = SimpleAdapter(this, data, R.layout.list_item, names, ids)
        listeView.adapter = adapter
        listeView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            val intent = Intent(this@ListActivity, InfoActivity::class.java)
            intent.putExtra("titel", data[i][names[0]])
            intent.putExtra("authors", data[i][names[1]])
            intent.putExtra("seitenZahl", data[i][names[2]])
            intent.putExtra("Info", data[i][names[3]])
            intent.putExtra("bild", data[i][names[4]])
            startActivity(intent)
        }
    }
}
