package de.fewycoding.buchsuche

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import java.util.HashMap
import java.util.concurrent.ExecutionException

class MainActivity : AppCompatActivity() {
    companion object{
        lateinit var progressDialog: Dialog
        fun showProgressDialog(showProgress: Boolean) {
            if (showProgress) progressDialog.show()
            else progressDialog.dismiss()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressDialog = Dialog(this,R.style.MyAlertDialogStyle)
        progressDialog.setContentView(R.layout.dialog_custom_progress)
        btnStarteSuche.setOnClickListener { starteSuchenClick() }
    }

    fun starteSuchenClick() {
        if (editText?.text.toString().trim().isEmpty()) {
            val toast = Toast.makeText(this, "Bitte Suchbegriff eingeben", Toast.LENGTH_LONG)
            toast.show()
        } else {
            val data =listeBefuehlen(ArrayList(),editText.text.toString())
            if (data.isEmpty()){
                Toast.makeText(this, "Es gibt keine Bücher zu diesem Suchbegriff", Toast.LENGTH_LONG).show()
                return
            }
            val apiResult = ApiResults(data)
            val intent = Intent(this, ListActivity::class.java)
            intent.putExtra("apiResults", apiResult)
            startActivity(intent)
        }
    }


    fun listeBefuehlen(data: MutableList<Map<String, String>>, suchbegriff: String): List<Map<String, String>> {
        try {
            val apiResponse = GoogleAnfrage().execute(suchbegriff).get()
            for (i in 0 until apiResponse.totalItems) {
                val row = HashMap<String, String>()
                row[ListActivity.names[0]] = apiResponse.getTitle(i)
                row[ListActivity.names[1]] = apiResponse.getAuthors(i)
                row[ListActivity.names[2]] = "Seitenzahl: " + apiResponse.getPageCount(i).toString()
                row[ListActivity.names[3]] = apiResponse.getInfotext(i)
                row[ListActivity.names[4]] = apiResponse.getBildURL(i)
                data.add(row)
            }
        } catch (e: ExecutionException) {
            e.printStackTrace()
            Toast.makeText(this, "Liste konnte nicht geladen werden", Toast.LENGTH_LONG).show()

        } catch (e: InterruptedException) {
            e.printStackTrace()
            Toast.makeText(this, "Liste konnte nicht geladen werden", Toast.LENGTH_LONG).show()

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return data
    }

}





