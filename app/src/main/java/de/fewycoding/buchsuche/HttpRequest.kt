package de.fewycoding.buchsuche

import java.net.URL
import java.io.IOException
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder

class HttpRequest {


    companion object {
    @Throws(IOException::class)
     fun request(urlString: String): String {
        val url = URL(urlString)
        val connection = url.openConnection()
        connection.connectTimeout = 5000
        connection.readTimeout = 10000
        val buffer = BufferedReader(InputStreamReader(connection.getInputStream()))
        val builder = StringBuilder()
        buffer.forEachLine { builder.append(it) }
        return builder.toString()
    }
}}