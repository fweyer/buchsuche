package de.fewycoding.buchsuche

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ApiResults(var data: List<Map<String, String>>):Parcelable