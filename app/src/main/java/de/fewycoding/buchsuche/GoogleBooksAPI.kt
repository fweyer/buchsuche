package de.fewycoding.buchsuche

import java.net.URLEncoder
import org.json.JSONObject
import org.json.JSONException

class GoogleBooksAPI (queryString: String){

    val bookdata: JSONObject

    // der Request liefert nur die ersten 10 Ergebnisse zurück
    val totalItems: Int
    get() {
        val totalItems = bookdata.getInt("totalItems")
        return if (totalItems < 10)
            totalItems
        else
            10
    }

    init {
        val result = HttpRequest.request(baseURL + URLEncoder.encode(queryString, "UTF-8"))
        bookdata = JSONObject(result)
    }

    @Throws(JSONException::class)
    fun getTitle(index: Int): String {
        val items = bookdata.getJSONArray("items")
        val volumeInfo = items.getJSONObject(index).getJSONObject("volumeInfo")
        return volumeInfo.getString("title")
    }

    @Throws(JSONException::class)
    fun getAuthors(index: Int): String {
        val items = bookdata.getJSONArray("items")
        val volumeInfo = items.getJSONObject(index).getJSONObject("volumeInfo")
        val authors = volumeInfo.getJSONArray("authors")
        val builder = StringBuilder()
        builder.append(authors.getString(0))
        for (i in 1 until authors.length()) {
            builder.append(", " + authors.getString(i))
        }
        return builder.toString()
    }

    @Throws(JSONException::class)
    fun getPageCount(index: Int): Int {
        val items = bookdata.getJSONArray("items")
        val volumeInfo = items.getJSONObject(index).getJSONObject("volumeInfo")
        val page = volumeInfo.getString("pageCount")
        return Integer.valueOf(page)
    }

    fun getInfotext(index: Int): String {
        try {
            val items = bookdata.getJSONArray("items")
            val volumeInfo = items.getJSONObject(index).getJSONObject("volumeInfo")
            return volumeInfo.getString("description")
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return "Keine Buchbeschreinbung vorhanden"
    }

    fun getBildURL(index: Int): String {
        try {
            val items = bookdata.getJSONArray("items")
            val volumeInfo = items.getJSONObject(index).getJSONObject("volumeInfo")
            val image = volumeInfo.getJSONObject("imageLinks")
            return image.getString("thumbnail")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return "Keine Bild vorhanden"
    }

    companion object {
        private val APIKey = "AIzaSyAo6lGiDDIYiV6WRp16cPB1MXktduCRodQ"
        internal val baseURL = "https://www.googleapis.com/books/v1/volumes?key=$APIKey&q="
    }

}