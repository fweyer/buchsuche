package de.fewycoding.buchsuche

import android.os.AsyncTask
import android.util.Log
import org.json.JSONException
import java.io.IOException


class GoogleAnfrage : AsyncTask<String, Void, GoogleBooksAPI>() {

    override fun onPreExecute() {
        super.onPreExecute()
        MainActivity.showProgressDialog(true)
    }

    override fun doInBackground(vararg params: String): GoogleBooksAPI? {
        var api: GoogleBooksAPI? = null
        try {
            api = GoogleBooksAPI(params[0])
            for (i in 0 until api.totalItems) {
                Log.i("Titel: ", api.getTitle(i))
                Log.i("Autoren: ", api.getAuthors(i))
                Log.i("Seiten: ", api.getPageCount(i).toString())
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return api
    }

    override fun onPostExecute(result: GoogleBooksAPI?) {
        super.onPostExecute(result)
        MainActivity.showProgressDialog(false)
    }

}


