package de.fewycoding.buchsuche

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import org.hamcrest.CoreMatchers.anything
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class BuchsucheViewTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testStringJava() {
        textEingabeTest("Java")
        listAuswahlClick(1)
        Espresso.pressBack()
        listAuswahlClick(0)
    }

    @Test
    fun testStringPython() {
        textEingabeTest("Python")
        listAuswahlClick(2)
        Espresso.pressBack()
        listAuswahlClick(3)
    }

    @Test
    fun testStringAndroid() {
        textEingabeTest("Android")
        listAuswahlClick(2)
        Espresso.pressBack()
    }

    @Test
    fun testStringLeer() {
        textEingabeTest("   ")
    }

    private fun textEingabeTest(inputText: String) {
        Thread.sleep(200)
        onView(withId(R.id.editText)).perform(click(), typeText(inputText))
        Thread.sleep(200)
        onView(withId(R.id.btnStarteSuche)).perform(click())
        Thread.sleep(200)
    }

    private fun listAuswahlClick(index: Int) {
        Thread.sleep(200)
        onData(anything()).inAdapterView(withId(R.id.listeView)).atPosition(index).perform(click())
    }

}
